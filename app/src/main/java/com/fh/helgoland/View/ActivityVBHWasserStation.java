package com.fh.helgoland.View;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.widget.MediaController;
import android.widget.VideoView;

import com.fh.helgoland.R;

/**
 * Created by vikash on 30/11/16.
 */

public class ActivityVBHWasserStation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_vbhwasser);


        VideoView vbhWaserVideo = (VideoView)findViewById(R.id.wasser_video);

        //Creating MediaController
        MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(vbhWaserVideo);

        Uri uriPath = Uri.parse("android.resource://com.fh.helgoland/" + R.raw.videoplayback);



        //Setting MediaController and URI, then starting the videoView
        vbhWaserVideo.setMediaController(mediaController);
        vbhWaserVideo.setVideoURI(uriPath);
        vbhWaserVideo.requestFocus();
        vbhWaserVideo.start();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
