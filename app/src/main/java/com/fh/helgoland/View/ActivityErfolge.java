package com.fh.helgoland.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

import com.fh.helgoland.MainActivity;
import com.fh.helgoland.Model.Question;
import com.fh.helgoland.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class ActivityErfolge extends ActionBarActivity {

     private GridView gridView;
     private GridViewAdapter gridAdapter;
     int getCount = 0;
     Question[] questions = null;
     int currentStationErfolge= 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_erfolge);

             Question.initializeStations(this);

               JSONArray questionArray = Question.geQuestionJSONArray();
               questions = new Question[questionArray.length()];

                  for (int i = 0; i<questionArray.length(); i++) {
                         try {
                            questions[i] = new Question(questionArray.getJSONObject(i));
                         }
                         catch (JSONException jsonException) {
                             jsonException.printStackTrace();
                            }
                  }



            Toolbar coustmizeToolbar = (Toolbar)findViewById(R.id.toolbarCoustomize);
            setSupportActionBar(coustmizeToolbar);
            coustmizeToolbar.setTitle(null);




            // call Grid view for images
            gridView = (GridView) findViewById(R.id.gridViewErfolge);
            gridAdapter = new GridViewAdapter(this, R.layout.grid_item_erfolge, getData());
            gridView.setAdapter(gridAdapter);


    }

           // Prepare some dummy data for gridview

             private ArrayList<ImageItem> getData() {
                final ArrayList<ImageItem> imageItems = new ArrayList<>();
                TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
                TypedArray colorimgs = getResources().obtainTypedArray(R.array.color_image_ids);

                 for (int i = 0; i < imgs.length(); i++) {
                     Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imgs.getResourceId(i, -1));
                     Bitmap coloredImage = BitmapFactory.decodeResource(getResources(), colorimgs.getResourceId(i, -1));
                     imageItems.add(new ImageItem(bitmap,coloredImage, "Image#"  + i ));
                 }
                 return imageItems;
             }

                // on click function set on toolbar ro go to main activity
                public  void  MenuScreen(View v){
                    Intent intentStation = new Intent(ActivityErfolge.this ,MainActivity.class);
                    startActivity(intentStation);

                }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
