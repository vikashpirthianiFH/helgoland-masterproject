package com.fh.helgoland.View;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fh.helgoland.Model.Question;
import com.fh.helgoland.R;

import java.util.ArrayList;

/**
 * Created by vikash on 22/01/17.
 */

public class QuizOptionArrayAdapter extends ArrayAdapter<Question> {

    private Question[] questions;
    private Context context;

    public QuizOptionArrayAdapter(Context context, int resource, Question[] questions) {
        super(context, R.layout.options_layout);
        this.questions = questions;
        this.context = context;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Question getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public int getPosition(Question item) {
        return super.getPosition(item);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View option;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            option = new View(context);
            option = inflater.inflate(R.layout.options_layout, null);
            TextView textView = (TextView) option.findViewById(R.id.vikashText);
        } else {
            option = (View) convertView;
        }
        return option;
    }

}
