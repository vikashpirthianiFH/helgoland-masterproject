package com.fh.helgoland.View;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.fh.helgoland.Model.Stations;
import com.fh.helgoland.R;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.fh.helgoland.R.id.stations;

public class ActivityQrCodeScanner extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    int i = 0;
   public static final int test = 0;
    Stations[] stations = null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code_scanner);



        Stations.initializeStations(this);      // initialize the the station array

        JSONArray stationArray = Stations.getStationJSONArray();
        stations = new Stations[stationArray.length()];
        for (int i = 0; i<stationArray.length(); i++) {
            try {
                stations[i] = new Stations(stationArray.getJSONObject(i));
            }
            catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
        }



        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);

//        int permissionCheck = ContextCompat.checkSelfPermission(ActivityQrCodeScanner.this,Manifest.permission.CAMERA);
        if(ContextCompat.checkSelfPermission(ActivityQrCodeScanner.this,Manifest.permission.CAMERA )!= PackageManager.PERMISSION_GRANTED ){

            if(ActivityCompat.shouldShowRequestPermissionRationale(ActivityQrCodeScanner.this , Manifest.permission.CAMERA)){

            }  else{
                ActivityCompat.requestPermissions(ActivityQrCodeScanner.this , new String[]{Manifest.permission.CAMERA },  test);
            }

        }else{
            mScannerView.startCamera();
        }

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
       // mScannerView.startCamera();         // Start camera

    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void onResume(){
        super.onResume();
        mScannerView.resumeCameraPreview(this);
        mScannerView.startCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here

        Log.e("handler", rawResult.getText().toString()); // Prints scan results
        Log.e("handler", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode)

        // show the scanner result into dialog box.

        mScannerView.resumeCameraPreview(this);

        if(rawResult.getText().toString().equals( "google")){

           Intent station_intent_one = new Intent (ActivityQrCodeScanner.this , HelgolandData.class);
            station_intent_one.putExtra("station", stations[0]);
            station_intent_one.putExtra("currentStation", 0);

            startActivity(station_intent_one);

        }else if(rawResult.getText().toString().equals( "Station 2"))  {
            Intent station_intent_one = new Intent (ActivityQrCodeScanner.this , HelgolandData.class);
            station_intent_one.putExtra("station", stations[1]);
            station_intent_one.putExtra("currentStation", 0);

            startActivity(station_intent_one);


        }else
        {
            Toast toast = Toast.makeText(getApplicationContext(),  "Please scan valid QR code ", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case test: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    mScannerView.startCamera();
                } else {

                    
                    mScannerView.stopCameraPreview();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}