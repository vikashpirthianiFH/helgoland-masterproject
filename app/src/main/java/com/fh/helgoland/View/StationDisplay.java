package com.fh.helgoland.View;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.fh.helgoland.MainActivity;
import com.fh.helgoland.Model.Question;
import com.fh.helgoland.Model.Stations;
import com.fh.helgoland.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Created by vikash on 03/12/16.
 */

public class StationDisplay extends ActionBarActivity {

    String [] stationsArray = {"Rathaus Startpunkt","WindMW", "VBH Strom", "VBH Wärme","VBH Trinkwasser","Innogy SE","E.ON","VBH Zero Emission", "Lng"};
    Stations[] stations = null;
    Question[] questions =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stations_list);

        //couztmized toolbar
        Toolbar coustmizeToolbar = (Toolbar) findViewById(R.id.toolbarCoustomize);
        setSupportActionBar(coustmizeToolbar);
        coustmizeToolbar.setTitle(null);

// To initialize the question and options  and get the parsed JsonArray and JsonObject
        Question.initializeStations(this);
        JSONArray questionArray = Question.geQuestionJSONArray();
        questions = new Question[questionArray.length()];
        for (int i = 0; i<questionArray.length(); i++) {
            try {
                questions[i] = new Question(questionArray.getJSONObject(i));
            }
            catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
        }
        // To initialize the Station and get the parsed JsonArray and JsonObject
        Stations.initializeStations(this);
        JSONArray stationArray = Stations.getStationJSONArray();
        stations = new Stations[stationArray.length()];
        for (int i = 0; i<stationArray.length(); i++) {
            try {
                stations[i] = new Stations(stationArray.getJSONObject(i));
            }
            catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
        }


            ArrayAdapter stationAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview_stations, stationsArray);

            ListView listView = (ListView) findViewById(R.id.stations_list);
            listView.setAdapter(stationAdapter);


            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    if (position == 4){

                        Intent intentWasserStation = new Intent(StationDisplay.this, ActivityVBHWasserStation.class);
                        startActivity(intentWasserStation);

                    }
                    else if( position >= 4){

                        Intent intent = new Intent(StationDisplay.this, HelgolandData.class);
                        intent.putExtra("station", stations[--position]);
                        intent.putExtra("currentStation", position);
                        intent.putExtra("stationCount", stations.length);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(StationDisplay.this, HelgolandData.class);
                        intent.putExtra("station", stations[position]);
                        intent.putExtra("currentStation", position);
                        intent.putExtra("stationCount", stations.length);
                        startActivity(intent);
                    }
                }
            });

    } @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

    // To go fro menu screen
    public  void  MenuScreen(View v){
        Intent intentStation = new Intent(StationDisplay.this ,MainActivity.class);
        startActivity(intentStation);

    }

}


