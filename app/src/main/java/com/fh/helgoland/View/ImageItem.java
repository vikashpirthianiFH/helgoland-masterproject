package com.fh.helgoland.View;

import android.graphics.Bitmap;

/**
 * Created by vikash on 02/02/17.
 */
public class ImageItem {
    private Bitmap image;
    private Bitmap colorImage;


    public ImageItem(Bitmap image,  Bitmap colorImage, String s) {
        super();
        this.image = image;
        this.colorImage = colorImage;
    }
    public Bitmap getColorImage(){
        return colorImage;
    }
    public Bitmap getImage() {
        return image;
    }
    public void setColorImage(Bitmap colorImage) {
        this.colorImage = colorImage;
    }
    public void setImage(Bitmap image) {
        this.image = image;
    }

}