package com.fh.helgoland.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fh.helgoland.MainActivity;
import com.fh.helgoland.Model.Stations;
import com.fh.helgoland.R;

import org.json.JSONArray;
import org.json.JSONException;

public class ActivityInfoFromStations extends AppCompatActivity {

    Stations[] stations = null;
    Stations station;
    int currentStation = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_from_stations);
        Intent intent = getIntent();


        // To initialize the Station and get the parsed JsonArray and JsonObject
        Stations.initializeStations(this);
        JSONArray stationArray = Stations.getStationJSONArray();
        stations = new Stations[stationArray.length()];
        for (int i = 0; i<stationArray.length(); i++) {
            try {
                stations[i] = new Stations(stationArray.getJSONObject(i));
            }
            catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
        }




            currentStation = intent.getIntExtra("currentStation", 0);
            station = stations[currentStation];


            TextView headingInfo = (TextView) findViewById(R.id.infoID);
            headingInfo.setText(station.getTitle());

            TextView infoTextView = (TextView) findViewById(R.id.infoTextId);
            infoTextView.setText(station.getInfoText());


            ImageView backToStationView = (ImageView) findViewById(R.id.infoBackToStation);

            backToStationView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    Intent station_intent_one = new Intent(ActivityInfoFromStations.this, HelgolandData.class);
                    station_intent_one.putExtra("station", stations[currentStation]);
                    station_intent_one.putExtra("currentStation", 0);

                    startActivity(station_intent_one);
                }
            });



    }
    public  void  MenuScreen(View v){
        Intent intentStation = new Intent(ActivityInfoFromStations.this ,MainActivity.class);
        startActivity(intentStation);

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }}
