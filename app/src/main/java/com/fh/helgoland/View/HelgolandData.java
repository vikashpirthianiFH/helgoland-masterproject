package com.fh.helgoland.View;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fh.helgoland.MainActivity;
import com.fh.helgoland.Model.Question;
import com.fh.helgoland.R;

import com.fh.helgoland.Model.Stations;

import org.json.JSONArray;
import org.json.JSONException;
import org.xml.sax.ErrorHandler;

public class HelgolandData extends AppCompatActivity {


    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private int currentStation = 0;
    Question [] questions = null;
    private Stations station;
    private int maxStations= -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rathaus_pager);
        Toolbar coustmizeToolbar = (Toolbar)findViewById(R.id.toolbarCoustomizeStation);
        coustmizeToolbar.setTitle("");
        setSupportActionBar(coustmizeToolbar);
        coustmizeToolbar.setTitle(null);

        Intent intent = getIntent();



        Stations station = (Stations) intent.getExtras().getSerializable("station");
        currentStation = intent.getIntExtra("currentStation", 0);
        maxStations = intent.getIntExtra("stationCount", 0);


        if(currentStation == 0){
            ImageView infofirstStation = (ImageView)findViewById(R.id.infoImageStations);
            infofirstStation.setVisibility(viewPager.GONE);
        }

        viewPager = (ViewPager) findViewById(R.id.rathaus_pager);
        myViewPagerAdapter = new MyViewPagerAdapter(station);
        viewPager.setAdapter(myViewPagerAdapter);



        Question.initializeStations(this);
        JSONArray questionArray = Question.geQuestionJSONArray();
        questions = new Question[questionArray.length()];
        for (int i = 0; i<questionArray.length(); i++) {
            try {
                questions[i] = new Question(questionArray.getJSONObject(i));
            }
            catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
        }
    }

    public  void  MenuScreen(View v){
        Intent intentStation = new Intent(HelgolandData.this ,MainActivity.class);
        startActivity(intentStation);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }
    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter  {
        private LayoutInflater layoutInflater;
        private Stations station;

        public MyViewPagerAdapter(Stations stations) {
            this.station = stations;

        }

        public void changeStation(Stations station) {
            this.station = station;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view1 = null;


            if (position == station.getText().length){
                view1 = layoutInflater.inflate(R.layout.activity_to_next_options, null );

                TextView quizView = (TextView)view1.findViewById(R.id.to_quiz);
                quizView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                    Intent intentQuiz = new Intent(view.getContext() ,ActvityQuizPager.class);
                        intentQuiz.putExtra("currentStation", currentStation);
                         startActivity(intentQuiz);

                    }
                });





                final TextView nextStation = (TextView)view1.findViewById(R.id.next_station);

                nextStation.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        if(currentStation == (maxStations-1)|| maxStations ==-1) {

                            nextStation.setText("Go to Menu");
                            Intent intentMain = new Intent(HelgolandData.this, MainActivity.class);
                            startActivity(intentMain);
                        }else {
                            station = Stations.getStation(++currentStation);
                            myViewPagerAdapter.changeStation(station);
                            myViewPagerAdapter.notifyDataSetChanged();
                            viewPager.setCurrentItem(0, false);

                        }
                    }
                });

            }
            else {
               view1 = layoutInflater.inflate(R.layout.swipe_rathaus , container, false);

                TextView heading = (TextView)view1.findViewById(R.id.station_heading);
                heading.setText(station.getTitle());

                TextView contentText = (TextView)view1.findViewById(R.id.station_content);
                contentText.setText(station.getText()[position].toString());

                String uri = "@drawable/"+station.getImage()[position].toString();  // where myresource (without the extension) is the file

                 int imageResource = getResources().getIdentifier(uri, null, getPackageName());

                ImageView imageView = (ImageView)view1.findViewById(R.id.imageStation);
                Drawable res = getResources().getDrawable(imageResource);
                imageView.setImageDrawable(res);
            }

            final ImageView leftScreenView = (ImageView)findViewById(R.id.leftScreen);
            leftScreenView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    viewPager.setCurrentItem(viewPager.getCurrentItem() -1, true);
            }
        });

            final ImageView rightScreenView = (ImageView)findViewById(R.id.rightScreen);
            rightScreenView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    viewPager.setCurrentItem(viewPager.getCurrentItem() +1, true);
                }
            });

            final ImageView infoImageView = (ImageView)findViewById(R.id.infoImageStations);
            infoImageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    Intent infoIntent =  new Intent(HelgolandData.this ,ActivityInfoFromStations.class);
                    infoIntent.putExtra("currentStation", currentStation);
                    startActivity(infoIntent);
                }
            });

            container.addView(view1);

            return view1;
        }



        @Override
        public int getCount() {
            return station.getText().length + 1;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Toast toast = Toast.makeText(getApplicationContext(), "Ready to Go back ", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }


}


