package com.fh.helgoland.View;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.fh.helgoland.R;

import java.util.ArrayList;

/**
 * Created by vikash on 02/02/17.
 */
public class GridViewAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private ArrayList data = new ArrayList();

    public GridViewAdapter(Context context, int layoutResourceId, ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();

            holder.image = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }


        ImageItem item = (ImageItem) data.get(position);

            SharedPreferences settings = context.getSharedPreferences("YOUR_PREF_NAME", 0);
           boolean isImageColored = settings.getBoolean(position+"", false);

//
//        for :
//             ) {
//
//        }
        if (isImageColored){
            holder.image.setImageBitmap(item.getColorImage());
        }else{
            holder.image.setImageBitmap(item.getImage());
        }



        return row;
    }

    static class ViewHolder {

        ImageView image;
    }

}