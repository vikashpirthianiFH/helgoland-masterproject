package com.fh.helgoland.View;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fh.helgoland.MainActivity;
import com.fh.helgoland.Model.Stations;
import com.fh.helgoland.R;

import org.json.JSONArray;
import org.json.JSONException;

public class ActivityRundgang extends ActionBarActivity {

    private ViewPager viewPagerRundgag;
    private MyRundgangViewPagerAdapter myViewPagerAdapterRundgang;
    Stations[] stations = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rundgang_pager);

        // To initialize the Station and get the parsed JsonArray and JsonObject
        Stations.initializeStations(this);
        JSONArray stationArray = Stations.getStationJSONArray();
        stations = new Stations[stationArray.length()];
        for (int i = 0; i<stationArray.length(); i++) {

                try {
                    stations[i] = new Stations(stationArray.getJSONObject(i));
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();}

        }


        viewPagerRundgag = (ViewPager) findViewById(R.id.rundgang_pager);
        myViewPagerAdapterRundgang = new MyRundgangViewPagerAdapter(stations[1]);
        viewPagerRundgag.setAdapter(myViewPagerAdapterRundgang);


        viewPagerRundgag.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });
    }

    public  void  QrScanner(View v){
        Intent intentQrScanner = new Intent(ActivityRundgang.this ,ActivityQrCodeScanner.class);
        startActivity(intentQrScanner);

    }
    public  void  MenuScreen(View v){
        Intent intentStation = new Intent(ActivityRundgang.this ,MainActivity.class);
        startActivity(intentStation);

    }

    public class MyRundgangViewPagerAdapter extends PagerAdapter{
        private Stations station;
        private LayoutInflater layoutInflater;

        public MyRundgangViewPagerAdapter(Stations stations) {
            this.station = stations;

        }




        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            View viewRundgang = layoutInflater.inflate(R.layout.activity_rundgang, container, false);
            final TextView nextStationView = (TextView)viewRundgang.findViewById(R.id.nextRundgang);

            Toolbar coustmizeToolbar = (Toolbar)viewRundgang.findViewById(R.id.toolbarCoustomize);
            setSupportActionBar(coustmizeToolbar);
            coustmizeToolbar.setTitle(null);

            if(position ==  9){   nextStationView.setText("Rundgang Beende");

              nextStationView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view2) {

                        Intent intentStation = new Intent(ActivityRundgang.this ,MainActivity.class);
                        startActivity(intentStation);
                    }

                });
          }
            else {
                nextStationView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view2) {

                        viewPagerRundgag.setCurrentItem(viewPagerRundgag.getCurrentItem()+1, true);

                    }

                });
          }


            String uri = "@drawable/"+station.getMap()[position].toString();  // where myresource (without the extension) is the file

            int mapResource = getResources().getIdentifier(uri, null, getPackageName());

            ImageView  mapView = (ImageView)viewRundgang.findViewById(R.id.imageRundgangStation);
            Drawable res = getResources().getDrawable(mapResource);
            mapView.setImageDrawable(res);

            container.addView(viewRundgang);

            return viewRundgang;
        }

        @Override
        public int getCount() {
            return station.getMap().length;

        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

}
