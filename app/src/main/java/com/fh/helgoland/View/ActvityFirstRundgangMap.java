package com.fh.helgoland.View;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.fh.helgoland.MainActivity;
import com.fh.helgoland.Model.Stations;
import com.fh.helgoland.R;

public class ActvityFirstRundgangMap extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_first_rundgang_map);

        //couztmized toolbar
        Toolbar coustmizeToolbar = (Toolbar) findViewById(R.id.toolbarCoustomize);
        setSupportActionBar(coustmizeToolbar);
        coustmizeToolbar.setTitle(null);


    }

    public void goToIntroduction(View view) {
        setContentView(R.layout.introduction_rundgang);

    }

    public void goToNextMaps(View view) {
        Intent intentNextStation = new Intent(ActvityFirstRundgangMap.this, ActivityRundgang.class);
        startActivity(intentNextStation);

    }

    public void MenuScreen(View v) {
        Intent intentStation = new Intent(ActvityFirstRundgangMap.this, MainActivity.class);
        startActivity(intentStation);
    }

}
