package com.fh.helgoland.View;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fh.helgoland.MainActivity;
import com.fh.helgoland.Model.Question;
import com.fh.helgoland.Model.Stations;
import com.fh.helgoland.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;

import static com.fh.helgoland.R.id.decor_content_parent;
import static com.fh.helgoland.R.id.gridView;
import static com.fh.helgoland.R.id.stations;
import static java.lang.Thread.sleep;

public class ActvityQuizPager extends ActionBarActivity {



    private ViewPager viewPagerQuiz;
    private MyQuizViewPagerAdapter myViewPagerAdapterQuiz;
     int countQuestion = 0;

    TextView optionOneText;
    TextView optionTwoText;
    TextView optionThreeText;
    TextView optionFourText;
    String text;
    RelativeLayout oneRelative;
    Question[] questions = null;
    private Question question;
    int currentQuestion= 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_quiz_pager);




// To initialize the Station and get the parsed JsonArray and JsonObject

      Question.initializeStations(this);
        JSONArray questionArray = Question.geQuestionJSONArray();
           questions = new Question[questionArray.length()];
            for (int i = 0; i<questionArray.length(); i++) {
                  try {
                        questions[i] = new Question(questionArray.getJSONObject(i));
                 } catch (JSONException jsonException) {
                        jsonException.printStackTrace();
                            }

            }



           Intent quizintent =    getIntent();
          currentQuestion =       quizintent.getIntExtra("currentStation", currentQuestion);



        // Read the Json file Station.json and convert it into String str;


      question =  Question.getQuestions(currentQuestion);

        viewPagerQuiz = (ViewPager) findViewById(R.id.quiz_pager);
        myViewPagerAdapterQuiz = new MyQuizViewPagerAdapter(question);
        viewPagerQuiz.setAdapter(myViewPagerAdapterQuiz);


        viewPagerQuiz.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });


    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }


    public class MyQuizViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;
        private Question question;

        public MyQuizViewPagerAdapter(Question question) {
            this.question = question;

        }

        @Override
        public Object instantiateItem(ViewGroup container,  int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final int pagerPosition = position;

             View view = layoutInflater.inflate(R.layout.swipe_quiz, container, false);


            final String correctOption = question.getCorrectAnswer()[position].toString();
            question.getCorrectAnswer()[position].length();

            Toolbar coustmizeToolbar = (Toolbar)view.findViewById(R.id.toolbarCoustomizeQuiz);
            setSupportActionBar(coustmizeToolbar);
            coustmizeToolbar.setTitle(null);


            final TextView questionText = (TextView) view.findViewById(R.id.question);
            questionText.setText(question.getQuestion()[position].toString());

            optionOneText = (TextView) view.findViewById(R.id.opteOne);
            optionOneText.setText(question.getOption()[position][0].toString());
            optionOneText.setVisibility(View.GONE);
            final String currentTextOne=  optionOneText.getText().toString();


            optionTwoText = (TextView) view.findViewById(R.id.optTwo);
            optionTwoText.setText(question.getOption()[position][1].toString());
            optionTwoText.setVisibility(View.GONE);
            final String currentTextTwo=optionTwoText.getText().toString();


            optionThreeText = (TextView) view.findViewById(R.id.optThree);
            optionThreeText.setText(question.getOption()[position][2].toString());
            optionThreeText.setVisibility(View.GONE);
            final String currentTextThree = optionThreeText.getText().toString();


            optionFourText = (TextView) view.findViewById(R.id.optFour);
            optionFourText.setText(question.getOption()[position][3].toString());
            optionFourText.setVisibility(View.GONE);
            final String currentTextFour =optionFourText.getText().toString();



            final GridView gridView;
             final String[] numbers = new String[]{ currentTextOne,currentTextTwo,currentTextThree , currentTextFour};

            gridView = (GridView)view.findViewById(R.id.gridView);


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(ActvityQuizPager.this,R.layout.options_layout, numbers){
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);

                    if (position == 0 || position ==3) {
                        view.setBackgroundColor(getResources().getColor(R.color.quiz_box));
                    }else if(position== 1 || position ==2){
                        view.setBackgroundColor(getResources().getColor(R.color.quiz));
                    }

                    return view;
                }
            };
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    TextView testGrid = (TextView)view.findViewById(R.id.vikashText);

                    Log.e("optionssss", correctOption);
                    text =  testGrid.getText().toString();
                    if(text.equalsIgnoreCase(correctOption)) {

                        view.setBackgroundColor(getResources().getColor(R.color.green));

                        Thread timer = new Thread() {

                            public void run() {

                                runOnUiThread(new Runnable() {

                                    public void run() {
                                        try {
                                            sleep(500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }

                                        viewPagerQuiz.setCurrentItem(viewPagerQuiz.getCurrentItem()+1, true);

                                    }
                                });
                            }

                        };
                        timer.start();

                    countQuestion++;
                 }
                    else{
                        view.setBackgroundColor(getResources().getColor(R.color.red));

                        Thread timer = new Thread() {

                            public void run() {

                                runOnUiThread(new Runnable() {

                                    public void run() {
                                        try {
                                            sleep(500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        viewPagerQuiz.setCurrentItem(viewPagerQuiz.getCurrentItem() + 1, true);


                                    }
                                });
                            }

                        };
                        timer.start();
                    }
                    if (question.getQuestion().length == pagerPosition + 1) {
                        Thread timer = new Thread() {

                            public void run() {

                                runOnUiThread(new Runnable() {

                                    public void run() {
                                        Intent intentStation = new Intent(ActvityQuizPager.this, ActivityFromQuizToNext.class);
                                        intentStation.putExtra("countQuestion", countQuestion);
                                        intentStation.putExtra("value", currentQuestion);

                                        if(countQuestion>=3){

                                            SharedPreferences stationSahredPreference = getSharedPreferences("YOUR_PREF_NAME", 0);
                                            SharedPreferences.Editor editor = stationSahredPreference.edit();
                                            editor.putBoolean(currentQuestion+"", true);
                                            editor.commit();
                                        }

                                        startActivity(intentStation);

                                    }
                                });
                            }

                        };
                        timer.start();
                    }
                }
            });

            gridView.setAdapter(adapter);

            boolean colorImage = false;


            SharedPreferences settings = getSharedPreferences("YOUR_PREF_NAME", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("stationOne", colorImage);

            editor.putInt("correctOptionFromQuiz",countQuestion);
            editor.commit();

            container.addView(view);

            return view;

        }

        public  void  MenuScreen(View v){
            Intent intentStation = new Intent(ActvityQuizPager.this ,MainActivity.class);
            startActivity(intentStation);

        }


        @Override
        public int getCount() {
            return question.getQuestion().length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


}
