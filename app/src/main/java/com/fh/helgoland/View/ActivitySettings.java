package com.fh.helgoland.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.fh.helgoland.MainActivity;
import com.fh.helgoland.R;

public class ActivitySettings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        RelativeLayout sprache_relative = (RelativeLayout)findViewById(R.id.sprache_setting);
        sprache_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSprache = new Intent(ActivitySettings.this ,ActivitySprache.class);
                startActivity(intentSprache);
            }
        });
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
