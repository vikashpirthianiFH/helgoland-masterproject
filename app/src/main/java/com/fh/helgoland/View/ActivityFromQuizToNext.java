package com.fh.helgoland.View;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fh.helgoland.MainActivity;
import com.fh.helgoland.Model.Question;
import com.fh.helgoland.Model.Stations;
import com.fh.helgoland.R;

import org.json.JSONArray;
import org.json.JSONException;

public class ActivityFromQuizToNext extends ActionBarActivity {


    int i = 0;
    int currentQuestion = 0;
    Stations[] stations = null;
    Question[] questions = null;
    int countQuestion = 0;
    int getCount = 0;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_from_quiz_to_next);


            SharedPreferences settings = getSharedPreferences("YOUR_PREF_NAME", 0);
            int getCorrectOptions = settings.getInt("correctOptionFromQuiz", 0);


            final Intent intentFrom = getIntent();
            currentQuestion = intentFrom.getIntExtra("value", currentQuestion);
            getCount =intentFrom.getIntExtra("countQuestion" , countQuestion);

          final TextView correctQuestion = (TextView)findViewById(R.id.questionsCheckingText);
            correctQuestion.setText( "Du hast "+ getCount+" von 5 Fragen gewusst und damit die Station erfolgreich bestanden.");

            Question.initializeStations(this);
            JSONArray questionArray = Question.geQuestionJSONArray();
            questions = new Question[questionArray.length()];
            for (int i = 0; i < questionArray.length(); i++) {
                try {
                 questions[i] = new Question(questionArray.getJSONObject(i));
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                }
            }

            Stations.initializeStations(this);
            final JSONArray stationArray = Stations.getStationJSONArray();
             stations = new Stations[stationArray.length()];
                 for (int i = 0; i < stationArray.length(); i++) {
                     try {
                     stations[i] = new Stations(stationArray.getJSONObject(i));
                        } catch (JSONException jsonException) {
                         jsonException.printStackTrace();
                              }
                 }


            final TextView nextStation = (TextView) findViewById(R.id.nextStationFromQuiz);

              nextStation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                Toast toast = Toast.makeText(getApplicationContext(), "Ready to Go for next Station ", Toast.LENGTH_SHORT);
                toast.show();
                if(currentQuestion == (stationArray.length()-1)|| stationArray.length() <=0) {

                    Intent intentMain = new Intent(ActivityFromQuizToNext.this, MainActivity.class);
                    nextStation.setText("Go to Menu");
                    startActivity(intentMain);
                }else {
                    Intent nextFromQuizIntent = new Intent(ActivityFromQuizToNext.this, HelgolandData.class);
                    nextFromQuizIntent.putExtra("station", stations[++currentQuestion]);
                    nextFromQuizIntent.putExtra("currentStation", 0);
                    startActivity(nextFromQuizIntent);
                }
            }
        });


          TextView repeatQuizText = (TextView) findViewById(R.id.repeatQuiz);

          repeatQuizText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intentQuiz = new Intent(view.getContext(), ActvityQuizPager.class);
                intentQuiz.putExtra("currentStation", currentQuestion);
                view.getContext().startActivity(intentQuiz);

            }
        });

           TextView toMyErfolgeText = (TextView) findViewById(R.id.toMyErfolge);

          toMyErfolgeText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent erfolgeIntent = new Intent(ActivityFromQuizToNext.this, ActivityErfolge.class);
                startActivity(erfolgeIntent);
            }
          });

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }


    public void MenuScreen(View v) {

        Intent intentStation = new Intent(ActivityFromQuizToNext.this, MainActivity.class);
        startActivity(intentStation);

    }



}