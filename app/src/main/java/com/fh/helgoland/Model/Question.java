package com.fh.helgoland.Model;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;

public class Question implements Serializable {

    JSONArray optionOfArray = null;
    String[][] opt = new String[5][4];
    private static String stationString = "";
    private static JSONArray questionArray;
    private String [] question;
    private String [][] options;
    private String userOption;
    private String []correctAnswer;


    public  Question(){
        question = new String[1] ;
        options= new String[5][4];
        correctAnswer =new String[1];
    }

    public Question(String[] question, String[][] options, String[] correctAnswer){
        this.question = question;
        this.options= options;
        this.correctAnswer = correctAnswer;
    }
    // copy Constructor
    public  Question(Question aQuestion){
        this.question = aQuestion.question;
        this.correctAnswer = aQuestion.correctAnswer;
        for (int i = 0 ;  i<aQuestion.options.length; i++){
            this.options[i]= aQuestion.options[i];
        }
    }

     public Question(JSONObject json){
         try {


             JSONArray questionJsonArray = json.getJSONArray("questions");

             String[] questionString = new String[questionJsonArray.length()];
             for(int j=0; j<questionJsonArray.length() ;j++){
                 questionString[j]= questionJsonArray.getJSONObject(j).get("question").toString();
             }
             setQuestion(questionString);

            JSONArray correctOptionsArray = json.getJSONArray("correct_answres");
             String[] correctOptionString = new String[correctOptionsArray.length()];

             for(int k=0; k<correctOptionsArray.length(); k++){
                 correctOptionString[k]= correctOptionsArray.getJSONObject(k).get("answer").toString();
             }
             setCorrectAnswer(correctOptionString);

             JSONArray optionsJsonArray = json.getJSONArray("optionsOfQuestions");
             String[] optionString = new String[optionsJsonArray.length()];

             for(int k = 0; k<optionsJsonArray.length(); k++) {
                 optionOfArray  = optionsJsonArray.getJSONArray(k);

               //  Log.e("Errorrrr Option12", optionOfArray.toString());
//
                 for(int l= 0; l<optionOfArray.length();l++){
                     opt[k][l] = optionOfArray.get(l).toString();

                 }
             }
             setOptions(opt);



         } catch (JSONException e) {
             e.printStackTrace();
         }

     }
    public void setQuestion(String[] question){
        this.question = question;
    }

    public String[] getQuestion(){
        return question;
    }

     public  void setCorrectAnswer(String [] correctAnswer){
         this.correctAnswer =correctAnswer;
     }

    public String[] getCorrectAnswer(){
        return correctAnswer;
    }


    public String[][] getOption(){
        return options;
    }

   public void setOptions(String [][] options){
      this.options =options;

   }

    public static Question getQuestions(int index) {
        Question question = null;
        try {
            question = new Question(questionArray.getJSONObject(index));
        }
        catch (JSONException jsonException) {

        }
        return  question;

    }

    // return the Json array
    public static JSONArray geQuestionJSONArray() {
        questionArray = new JSONArray();
        try {
            JSONObject json = new JSONObject(stationString);
            questionArray = json.getJSONArray("stations");
        }
        catch(JSONException jsonException) {

        }
        return questionArray;
    }



    //read the Json file and convert it into string
    public static void initializeStations(Context context) {

        try {
            AssetManager assetManager = context.getAssets();
            InputStream in = assetManager.open("stations.json");
            InputStreamReader isr = new InputStreamReader(in);
            char[] inputBuffer = new char[100];

            int charRead;
            while ((charRead = isr.read(inputBuffer)) > 0) {
                String readString = String.copyValueOf(inputBuffer, 0, charRead);
                stationString += readString;
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
