package com.fh.helgoland.Model;
import com.google.gson.annotations.SerializedName;
/**
 * Created by vikash on 01/12/16.
 */

public class Quiz {


    private String name;
    private String[] text;
    private Question[] questions;


    public  Quiz()
    {
        name = "";
        Question[] questions = new Question[1];
    }

    public Quiz(String name , String[] text,Question[] questions){
        this.name = name;
        this.text = text;
        this.questions = questions;

    }
    public void setText(String[] text){
        for(int i= 0 ;i<text.length; i++) {


            this.text[i] = text[i];
        }
    }

    public String[] getText(){
        return text;
    }

    public  void setStation(String name){
        this.name = name;
    }
    public String getStation(){
        return name;
    }

    public void setQuestions(Question[] questions) {
        this.questions = new Question[questions.length];
        for (int i=0; i<questions.length; i++) {
            this.questions[i] = new Question(questions[i]);
        }
    }

    public void setQuestion (Question question, int index){
        this.questions[index] = question;

        for (int i=0; i<questions.length; i++) {
            this.questions[i] = new Question(questions[i]);
        }
    }

    public Question getQuestion(int index) {
        return questions[index];
    }



}
