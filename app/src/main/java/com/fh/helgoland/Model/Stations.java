package com.fh.helgoland.Model;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;

/**
 * Created by vikash on 15/12/16.
 */

public class Stations implements Serializable {

    private static String stationString = "";
    private static JSONArray stationsArray;

    private String title;
    private String infoText;
    private String[] text;
    private String[]  map;
    private String[] image;
    private int audio [];


    public Stations() {
        infoText= "";
        title = "";
        map = new String[1];
        text = new String[1];
        image = new String[1];
        audio = new int[]{};

    }

    public Stations(String title, String infoText, String[] text, String[] image,String[] map, int audio[]) {
        this.title = title;
        this.infoText = infoText;
        this.text = text;
        this.image = image;
        this.audio = audio;
        this.map = map;

    }

    public Stations(JSONObject json) {
        try {
            setTitle(json.get("name").toString());
            setInfoText(json.get("infoText").toString());
            JSONArray images = json.getJSONArray("images");

            String[] imagesString = new String[images.length()];
            for (int j = 0; j < images.length(); j++) {
                imagesString[j] = images.getJSONObject(j).get("img").toString();
            }
            setImage(imagesString);


            JSONArray mapsArray = json.getJSONArray("maps");
            String[] mapsString = new String[10];

            for(int i =0 ;i < 10;i++){
                mapsString[i] = mapsArray.getJSONObject(i).get("map").toString();
            }
            setMap(mapsString);

            JSONArray text = json.getJSONArray("text");
            String[] textString = new String[text.length()];
            for (int k = 0; k < text.length(); k++) {
                textString[k] = text.getJSONObject(k).get("text").toString();
            }
            setText(textString);



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void setTitle(String title) {
        this.title = title;
    }
    public void setInfoText(String infoText){
        this.infoText = infoText;
    }
    public void setText(String[] text){
        this.text = text;
    }

    public void setImage(String[] image){
        this.image=image ;
    }
    public  void setMap(String[] map){
        this.map = map;
    }
    public String[] getMap(){
        return map;
    }
    public void setAudio(int[] audio){
        for(int i= 0 ;i<audio.length; i++) {

            this.audio[i] = audio[i];
        }
    }

    public String []getText(){
        return text;
    }
    public String getInfoText(){
        return infoText;
    }

    public String[] getImage(){
        return image;
    }

    public int [] getAudio(){
        return audio;
    }

    public String getTitle(){
        return title;
    }

    public static Stations getStation(int index) {
        Stations station = null;
        try {
            station = new Stations(stationsArray.getJSONObject(index));
        }
        catch (JSONException jsonException) {

        }
        return  station;

    }

    public static JSONArray getStationJSONArray() {
        stationsArray = new JSONArray();
        try {
            JSONObject json = new JSONObject(stationString);
            stationsArray = json.getJSONArray("stations");
        }
        catch(JSONException jsonException) {

        }
        return stationsArray;
    }

    public static void initializeStations(Context context) {

        try {
            AssetManager assetManager = context.getAssets();
            InputStream in = assetManager.open("stations.json");
            InputStreamReader isr = new InputStreamReader(in);
            char[] inputBuffer = new char[100];

            int charRead;
            while ((charRead = isr.read(inputBuffer)) > 0) {
                String readString = String.copyValueOf(inputBuffer, 0, charRead);
                stationString += readString;
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}

