package com.fh.helgoland;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fh.helgoland.Model.Question;
import com.fh.helgoland.Model.Stations;
import com.fh.helgoland.View.ActivityErfolge;
import com.fh.helgoland.View.ActivityRundgang;
import com.fh.helgoland.View.ActivitySettings;
import com.fh.helgoland.View.ActivityVBHWasserStation;
import com.fh.helgoland.View.ActvityFirstRundgangMap;
import com.fh.helgoland.View.ActvityQuizPager;
import com.fh.helgoland.View.SplashScreen;
import com.fh.helgoland.View.StationDisplay;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends ActionBarActivity {

    Stations[] stations = null;
    Question[] questions = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Question.initializeStations(this);
        JSONArray questionArray = Question.geQuestionJSONArray();
        questions = new Question[questionArray.length()];
        for (int i = 0; i<questionArray.length(); i++) {
            try {
                questions[i] = new Question(questionArray.getJSONObject(i));
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }

        }



        RelativeLayout station = (RelativeLayout)findViewById(R.id.station_menu);
        station.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentStation = new Intent(MainActivity.this ,StationDisplay.class);
                startActivity(intentStation);
            }
        });

        RelativeLayout rundgang = (RelativeLayout)findViewById(R.id.rundgang_menu);
        rundgang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRundgang = new Intent(MainActivity.this ,ActvityFirstRundgangMap.class);
                startActivity(intentRundgang);
            }
        });

        RelativeLayout settings = (RelativeLayout)findViewById(R.id.menu_settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSettings = new Intent(MainActivity.this ,ActivitySettings.class);
                startActivity(intentSettings);
            }
        });

        RelativeLayout erfolge = (RelativeLayout)findViewById(R.id.menu_erfolge);
        erfolge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentErfolge = new Intent(MainActivity.this ,ActivityErfolge.class);
                startActivity(intentErfolge);
            }
        });

    }
}
